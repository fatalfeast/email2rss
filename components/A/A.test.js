import React from 'react'
import { shallow } from 'enzyme'

import A from './A'

describe('A', () => {
  it('should render the A component', () => {
    const wrapper = shallow(<A href="/">click here</A>)

    expect(wrapper).toMatchSnapshot()
  })
})